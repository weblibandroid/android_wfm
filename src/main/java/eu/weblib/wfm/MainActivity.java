package eu.weblib.wfm;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;
import eu.weblib.wfm.util.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {

    private static final String TAG;
    private GridView gridView;
    private List<WeblibFileModel> mFilesList;
    private LruCache<String, Bitmap> memoryCache;

    /* renamed from: eu.weblib.wfm.MainActivity.1 */
    class C00001 implements OnItemClickListener {
        C00001() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            MainActivity.this.loadFile(position);
        }
    }

    /* renamed from: eu.weblib.wfm.MainActivity.2 */
    class C00012 extends LruCache<String, Bitmap> {
        C00012(int $anonymous0) {
            super($anonymous0);
        }

        protected int sizeOf(String key, Bitmap bitmap) {
            if (VERSION.SDK_INT > 12) {
                return bitmap.getByteCount() / 1024;
            }
            return (bitmap.getRowBytes() * bitmap.getHeight()) / 1024;
        }
    }

    public MainActivity() {
        this.mFilesList = null;
    }

    static {
        TAG = MainActivity.class.getSimpleName();
    }

    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "[WFM][Main] onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadData();
    }

    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "[WFM][Main] onNewIntent");
        setIntent(intent);
        loadData();
        super.onNewIntent(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_activities, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_quit:
                finish();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    private void loadData() {
        initData();
        buildFilesPath();
        if (this.mFilesList == null) {
            return;
        }
        if (this.mFilesList.size() == 1) {
            loadFile(0);
            finish();
            return;
        }
        configureCache();
        initGrid();
    }

    private void initData() {
        try {
            Intent intent = getIntent();
            Log.i(TAG, "[WFM][Main] Intent > " + intent);
            Log.i(TAG, "[WFM][Main] Extras > " + intent.getExtras());
            List<Map<String, String>> filesList = (List) intent.getSerializableExtra(Constants.EXTRA_FILES_ARRAY);
            this.mFilesList = new ArrayList();
            for (Map<String, String> file : filesList) {
                Log.d(TAG, "[WFM][Main] Init " + ((String) file.get(Constants.EXTRA_PATH)) + " (" + ((String) file.get(Constants.EXTRA_PATH)) + ")");
                this.mFilesList.add(new WeblibFileModel((String) file.get(Constants.EXTRA_FILE), (String) file.get(Constants.EXTRA_PATH)));
            }
        } catch (Exception e) {
            Log.e(TAG, "[WFM][Main] Error geting list from intent: " + e);
            Toast.makeText(this, "[WFM] Error loading files", Toast.LENGTH_SHORT).show();
            this.mFilesList = null;
        }
    }

    private void buildFilesPath() {
        if (this.mFilesList != null) {
            for (WeblibFileModel file : this.mFilesList) {
                String filePath = file.getPath();
                if (!(filePath == null || filePath.startsWith("/"))) {
                    file.setPath(new StringBuilder(String.valueOf(Environment.getExternalStorageDirectory().getPath())).append("/").append(filePath).toString());
                }
            }
        }
    }

    private void initGrid() {
        this.gridView = (GridView) findViewById(R.id.gridView1);
        this.gridView.setAdapter(new WeblibFileAdapter(this, this.mFilesList, this.memoryCache));
        this.gridView.setOnItemClickListener(new C00001());
    }

    private void loadFile(int position) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), ViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EXTRA_FILE, ((WeblibFileModel) this.mFilesList.get(position)).getName());
        bundle.putString(Constants.EXTRA_PATH, ((WeblibFileModel) this.mFilesList.get(position)).getPath());
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
        startActivity(intent);
    }

    private void configureCache() {
        this.memoryCache = new C00012(((int) (Runtime.getRuntime().maxMemory() / 1024)) / 8);
    }
}
