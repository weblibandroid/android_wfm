package eu.weblib.wfm.util;

import android.app.Activity;
import android.os.Build.VERSION;
import android.webkit.MimeTypeMap;

public class Utils {
    public static String getMimeType(String file) {
        String type = null;
        try {
            String extension = file.substring(file.lastIndexOf(".") + 1);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        } catch (Exception e) {
        }
        return type;
    }

    public static String getHumanName(String name) {
        if (name == null) {
            return null;
        }
        int start = 0;
        int end = name.length();
        if (name.contains("/")) {
            start = name.lastIndexOf("/");
        }
        if (name.contains(".")) {
            end = name.lastIndexOf(".");
        }
        return name.substring(start, end);
    }

    public static void hideStatusBar(Activity activity) {
        if (VERSION.SDK_INT >= 16) {
            activity.getWindow().getDecorView().setSystemUiVisibility(1798);
        }
        if (VERSION.SDK_INT >= 19) {
            activity.getWindow().getDecorView().setSystemUiVisibility(3846);
        }
    }
}
