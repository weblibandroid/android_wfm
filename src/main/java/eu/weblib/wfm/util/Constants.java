package eu.weblib.wfm.util;

public class Constants {
    public static final String EXTRA_FILE = "file";
    public static final String EXTRA_FILES_ARRAY = "wl_files";
    public static final String EXTRA_PATH = "path";
    public static final String MIME_IMAGE = "image/";
    public static final String MIME_PDF = "application/pdf";
    public static final String MIME_VIDEO = "video/";
}
