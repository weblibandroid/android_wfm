package eu.weblib.wfm;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import eu.weblib.wfm.pdf.MuPDFActivity;
import eu.weblib.wfm.util.Constants;
import eu.weblib.wfm.util.Utils;
import java.io.File;

public class ViewActivity extends Activity {
    private static final String TAG;

    /* renamed from: eu.weblib.wfm.ViewActivity.1 */
    class C00031 implements OnTouchListener {
        private final /* synthetic */ VideoView val$videoView;

        C00031(VideoView videoView) {
            this.val$videoView = videoView;
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                Log.d(ViewActivity.TAG, "[WFM] Video onTouch");
                if (this.val$videoView.isPlaying()) {
                    Log.d(ViewActivity.TAG, "[WFM] Video pause");
                    this.val$videoView.pause();
                } else {
                    this.val$videoView.start();
                    Log.d(ViewActivity.TAG, "[WFM] Video start");
                }
            }
            return true;
        }
    }

    /* renamed from: eu.weblib.wfm.ViewActivity.2 */
    class C00042 implements OnErrorListener {
        C00042() {
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.e(ViewActivity.TAG, "[WFM] Error reading video: " + what + ", " + extra);
            return false;
        }
    }

    /* renamed from: eu.weblib.wfm.ViewActivity.3 */
    class C00053 implements OnCompletionListener {
        C00053() {
        }

        public void onCompletion(MediaPlayer mp) {
        }
    }

    static {
        TAG = ViewActivity.class.getSimpleName();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        try {
            printContentFromIntent(findViewById(R.id.fullscreen_content));
        } catch (Exception e) {
            Log.e(TAG, "[WFM] Error : " + e);
            Toast.makeText(this, "[WFM] Error reading file. Check content...", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    protected void onResume() {
        Utils.hideStatusBar(this);
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_activities, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_full:
                Utils.hideStatusBar(this);
                return true;
            case R.id.action_quit:
                finish();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    private void printContentFromIntent(View v) {
        Bundle bundle = getIntent().getExtras();
        Log.d(TAG, "[WFM] " + bundle.toString());
        if (bundle != null) {
            String name = bundle.getString(Constants.EXTRA_FILE);
            String path = bundle.getString(Constants.EXTRA_PATH);
            setTitle(Utils.getHumanName(name));
            String type = Utils.getMimeType(path);
            Log.d(TAG, "[WFM] ==============");
            Log.d(TAG, "[WFM] file " + name);
            Log.d(TAG, "[WFM] path " + path);
            Log.d(TAG, "[WFM] type " + type);
            Log.d(TAG, "[WFM] ==============");
            if (type.equals(Constants.MIME_PDF)) {
                openPdfFile(path);
            } else if (type.startsWith(Constants.MIME_IMAGE)) {
                ((ViewGroup) v).addView(openImage(path));
            } else if (type.startsWith(Constants.MIME_VIDEO)) {
                ((ViewGroup) v).addView(openVideo(path));
            } else {
                TextView textView = new TextView(this);
                textView.setText("ERROR !!");
                ((ViewGroup) v).addView(textView);
            }
        }
    }

    private void openPdfFile(String path) {
        Log.d(TAG, "[WFM] Opening pdf file...");
        try {
            Intent intent = new Intent(this, MuPDFActivity.class);
            intent.setData(Uri.fromFile(new File(path)));
            intent.setAction("android.intent.action.VIEW");
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "[WFM] Error opening pdf file: " + e);
        }
    }

    private ImageView openImage(String path) {
        ImageView imageView = new ImageView(this);
        File imageFile = new File(path);
        if (imageFile.exists()) {
            imageView.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
        }
        return imageView;
    }

    private VideoView openVideo(String path) {
        VideoView videoView = new VideoView(this);
        videoView.setVideoPath(path);
        videoView.setOnTouchListener(new C00031(videoView));
        videoView.setOnErrorListener(new C00042());
        videoView.setOnCompletionListener(new C00053());
        videoView.start();
        return videoView;
    }
}
