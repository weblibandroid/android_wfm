package eu.weblib.wfm;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.weblib.wfm.util.Constants;
import eu.weblib.wfm.util.ImageUtils;
import eu.weblib.wfm.util.Utils;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

public class WeblibFileAdapter extends BaseAdapter {
    private static final String TAG;
    private Context context;
    private List<WeblibFileModel> files;
    LruCache<String, Bitmap> memoryCache;

    static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            this.bitmapWorkerTaskReference = new WeakReference(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return (BitmapWorkerTask) this.bitmapWorkerTaskReference.get();
        }
    }

    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        private int fileOrRes;
        private final WeakReference<ImageView> imageViewReference;
        private String key;

        public BitmapWorkerTask(ImageView imageView, int fileOrRes) {
            this.fileOrRes = -1;
            this.key = null;
            this.imageViewReference = new WeakReference(imageView);
            this.fileOrRes = fileOrRes;
        }

        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap;
            this.key = params[0];
            ImageView thumbnail = (ImageView) this.imageViewReference.get();
            if (this.fileOrRes == 1) {
                bitmap = ImageUtils.decodeSampledBitmapFromFile(this.key, thumbnail.getWidth(), thumbnail.getHeight());
            } else {
                bitmap = ImageUtils.decodeSampledBitmapFromRes(WeblibFileAdapter.this.context.getResources(), Integer.valueOf(this.key).intValue(), thumbnail.getWidth(), thumbnail.getHeight());
            }
            if (bitmap != null) {
                WeblibFileAdapter.this.addBitmapToMemoryCache(this.key, bitmap);
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            if (this.imageViewReference != null && bitmap != null) {
                ImageView imageView = (ImageView) this.imageViewReference.get();
                if (this == WeblibFileAdapter.getBitmapWorkerTask(imageView) && imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    static {
        TAG = WeblibFileAdapter.class.getSimpleName();
    }

    public WeblibFileAdapter(Context context, List<WeblibFileModel> files, LruCache<String, Bitmap> memoryCache) {
        this.context = null;
        this.files = null;
        this.memoryCache = null;
        this.context = context;
        this.files = files;
        this.memoryCache = memoryCache;
    }

    public int getCount() {
        return this.files.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
            convertView = new View(this.context);
            convertView = inflater.inflate(R.layout.file_item, null);
        }
        if (position % 4 == 2 || position % 4 == 1) {
            convertView.setBackgroundColor(Color.parseColor("#D6D6D6"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        TextView textView = (TextView) convertView.findViewById(R.id.text_file_item);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image_file_item);
        String name = ((WeblibFileModel) this.files.get(position)).getName();
        String path = ((WeblibFileModel) this.files.get(position)).getPath();
        textView.setText(name);
        String type = Utils.getMimeType(path);
        Log.i(TAG, "[WFM][Adapt] +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        Log.i(TAG, "[WFM][Adapt] Position: " + position);
        Log.i(TAG, "[WFM][Adapt] File: " + this.files.get(position));
        Log.i(TAG, "[WFM][Adapt] File content: " + ((WeblibFileModel) this.files.get(position)).getName());
        Log.i(TAG, "[WFM][Adapt] MIME: " + type);
        Log.i(TAG, "[WFM][Adapt] =============================================================");
        if (type == null) {
            loadBitmapFromResource(R.drawable.type_unknow, imageView);
        } else if (type.startsWith(Constants.MIME_IMAGE)) {
            loadBitmapFromFile(path, imageView);
        } else if (type.startsWith(Constants.MIME_VIDEO)) {
            loadBitmapFromResource(R.drawable.type_video, imageView);
        } else if (type.equals(Constants.MIME_PDF)) {
            loadBitmapFromResource(R.drawable.type_pdf, imageView);
        } else {
            loadBitmapFromResource(R.drawable.type_unknow, imageView);
        }
        return convertView;
    }

    public void loadBitmapFromFile(String path, ImageView imageView) {
        String imageKey = path;
        Bitmap bitmap = getBitmapFromMemCache(imageKey);
        if (!new File(path).isFile()) {
            loadBitmapFromResource(R.drawable.type_unknow, imageView);
        } else if (bitmap != null) {
            try {
                imageView.setImageBitmap(ImageUtils.getRoundedCornerBitmap(ThumbnailUtils.extractThumbnail(bitmap, 80, 80), 12));
            } catch (Exception e) {
                Log.e(TAG, "[WFM][Adapt] Error: " + e);
            }
        } else {
            Log.i(TAG, "[WFM][Adapt] No cache available");
            if (cancelPotentialWork(imageKey, imageView)) {
                BitmapWorkerTask task = new BitmapWorkerTask(imageView, 1);
                imageView.setImageDrawable(new AsyncDrawable(this.context.getResources(), bitmap, task));
                task.execute(new String[]{imageKey});
            }
        }
    }

    public void loadBitmapFromResource(int resId, ImageView imageView) {
        String imageKey = String.valueOf(resId);
        Bitmap bitmap = getBitmapFromMemCache(imageKey);
        if (bitmap != null) {
            try {
                imageView.setImageBitmap(bitmap);
                return;
            } catch (Exception e) {
                Log.e(TAG, "[WFM][Adapt] Error: " + e);
                return;
            }
        }
        Log.i(TAG, "[WFM][Adapt] No cache available");
        if (cancelPotentialWork(imageKey, imageView)) {
            BitmapWorkerTask task = new BitmapWorkerTask(imageView, 0);
            imageView.setImageDrawable(new AsyncDrawable(this.context.getResources(), bitmap, task));
            task.execute(new String[]{imageKey});
        }
    }

    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                return ((AsyncDrawable) drawable).getBitmapWorkerTask();
            }
        }
        return null;
    }

    public static boolean cancelPotentialWork(String key, ImageView imageView) {
        BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
        if (bitmapWorkerTask == null) {
            return true;
        }
        if (bitmapWorkerTask.key == key) {
            return false;
        }
        bitmapWorkerTask.cancel(true);
        return true;
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            this.memoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return (Bitmap) this.memoryCache.get(key);
    }
}
