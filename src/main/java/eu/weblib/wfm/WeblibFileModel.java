package eu.weblib.wfm;

import android.os.Parcel;
import android.os.Parcelable;

public class WeblibFileModel implements Parcelable {
    private String mName;
    private String mPath;

    public WeblibFileModel(String name, String path) {
        this.mName = null;
        this.mPath = null;
        this.mName = name;
        this.mPath = path;
    }

    public String getName() {
        return this.mName;
    }

    public String getPath() {
        return this.mPath;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setPath(String path) {
        this.mPath = path;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel arg0, int arg1) {
        arg0.writeString(this.mName);
        arg0.writeString(this.mPath);
    }
}
