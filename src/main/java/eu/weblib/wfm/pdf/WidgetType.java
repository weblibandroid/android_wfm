package eu.weblib.wfm.pdf;

public enum WidgetType {
	NONE,
	TEXT,
	LISTBOX,
	COMBOBOX
}
